//package com.in28minutes.rest.webservices.restfulwebservices.todo;
//
//import java.net.URI;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//
////import com.in28minutes.rest.webservices.restfulwebservices.todo.Todo;
//
//@RestController
//@CrossOrigin(origins = "http://localhost:4200")
//public class TodoResource {
//
//	@Autowired
//	private ToDoService todoService;
//
//	@GetMapping("/users/{username}/todos")
//	public List<Todo> getAllTodos(@PathVariable String username) throws InterruptedException {
//		Thread.sleep(500); // to demonstrate react life cycle, added a REST response delay
//		return todoService.findAll();
//	}
//
//	@GetMapping("/users/{username}/todos/{id}")
//	public Todo getTodo(@PathVariable String username, @PathVariable long id) {
//		return todoService.findById(id);
//	}
//
//	@DeleteMapping("/users/{username}/todos/{id}")
//	public ResponseEntity<Void> deleteTodo(@PathVariable String username, @PathVariable long id) {
//		Todo todo = todoService.deleteById(id);
//
//		// if successfully deleted, return http 204, no content
//		if (todo != null) {
//			return ResponseEntity.noContent().build();
//		}
//
//		// else return 404 not found
//		return ResponseEntity.notFound().build();
//	}
//
//	@PutMapping("/users/{username}/todos/{id}")
////	@PostMapping("/users/{username}/todos/{id}")
//	public ResponseEntity<Todo> updateTodo(@PathVariable String username, @PathVariable long id,
//			@RequestBody Todo todo) {
//
//		// original implementation: which only allows creating new Todos and makes them
//		// go out of order
//		// Todo todoUpdated = todoService.save(todo);
//		// return new ResponseEntity<Todo>(todoUpdated, HttpStatus.OK);
//
//		// allow updating in place
//		Todo todoUpdated = todoService.save(todo);
//		if (todoUpdated == null) {
//			return ResponseEntity.notFound().build();
//		} else {
//			return new ResponseEntity<Todo>(todoUpdated, HttpStatus.OK);
//		}
//	}
//
//	@PostMapping("/users/{username}/todos")
//	public ResponseEntity<Void> saveTodo(@PathVariable String username, @RequestBody Todo todo) {
//		System.out.println("saveTodo() called");
//		Todo todoCreated = todoService.saveNewTodo(todo);
//		if (todoCreated == null) {
//			return ResponseEntity.internalServerError().build();
//		} else {
//			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(todoCreated.getId())
//					.toUri();
//			return ResponseEntity.created(uri).build();
//		}
//
//	}
//}
