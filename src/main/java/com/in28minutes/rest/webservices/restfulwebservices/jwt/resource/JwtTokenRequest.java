package com.in28minutes.rest.webservices.restfulwebservices.jwt.resource;

import java.io.Serializable;

public class JwtTokenRequest implements Serializable {

	private static final long serialVersionUID = -5616176897013108345L;

	private String username;
	private String password;

	public JwtTokenRequest() {
		super();
	}

	public JwtTokenRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
        
//        {
//        	"token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpbjI4bWludXRlcyIsImV4cCI6MTYzNzI4ODk4NywiaWF0IjoxNjM2Njg0MTg3fQ.lzGah-wLn8Mmr6SoyDfklZY3fJtgXiKsy8aRum3APCm1NudtvGP77JQZWf2gjYlHWgXB87931A5WwE5VO7tjbQ"
//       	}
    }

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
