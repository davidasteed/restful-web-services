package com.in28minutes.rest.webservices.restfulwebservices.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ToDoService {
	@Autowired
	private TodoJpaRepository todoJpaRepository;
	
	public Todo save(Todo todo) {
		System.out.println("*** save() is updating todo.getId(): " + String.valueOf(todo.getId()));
		
		Todo todoToUpdate = todoJpaRepository.getById(todo.getId());
		todoToUpdate.setDescription(todo.getDescription());
		todoToUpdate.setDone(todo.isDone());
		todoToUpdate.setTargetDate(todo.getTargetDate());
		todoToUpdate.setUsername(todo.getUsername());
		todoJpaRepository.save(todoToUpdate);

		return todoToUpdate;
	}

	public Todo saveNewTodo(Todo todo) {
		return todoJpaRepository.save(todo);
	}
}
