package com.in28minutes.rest.webservices.restfulwebservices.todoHardcoded;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.ui.context.Theme;

@Service
public class ToDoHardCodedService {
	private static List<TodoHardCoded> todos = new ArrayList<>();
	private static int idCounter = 0;

	static {
		todos.add(new TodoHardCoded(idCounter, "in28minutes", "Learn to Dance 2", new Date(), false));
		todos.add(new TodoHardCoded(++idCounter, "in28minutes", "Learn about Microservices", new Date(), false));
		todos.add(new TodoHardCoded(++idCounter, "in28minutes", "Learn about Java", new Date(), false));
	}

	public List<TodoHardCoded> findAll() {
		return todos;
	}

	public TodoHardCoded deleteById(long id) {
		TodoHardCoded todo = findById(id);
		if (todo == null) {
			return null;
		}
		if (todos.remove(todo)) {
			return todo;
		}

		return null;
	}

	public TodoHardCoded findById(long id) {
		for (TodoHardCoded todo : todos) {
			if (todo.getId() == id) {
				return todo;
			}
		}

		return null;
	}

	public TodoHardCoded save(TodoHardCoded todo) {
//		if (todo.getId() == -1 || todo.getId() == 0) {
//			todo.setId(++idCounter);
//			todos.add(todo);
//		} else {
//			// shortcut method: just add a the new Todo. but this will create a new Todo
//			// with different Id
//			// which causes the frontend to list the new Todo out of order
//			// deleteById(todo.getId());
//			// todos.add(todo);
//			if ((int) todo.getId() >= 0 && todo.getId() <= todos.size()) {
//				todos.set((int) todo.getId() - 1, todo);
//			} else {
//				return null;
//			}
//		}
//		return todo;
		System.out.println("*** save(), todo.getId(): " + String.valueOf(todo.getId()));

		boolean foundTodo = false;
		for (int i = 0; i < todos.size(); i++) {
			if (todos.get(i).getId() == todo.getId()) {
				todos.set(i, todo);
				foundTodo = true;
				break;
			}
		}

		if (foundTodo == true) {
			return todo;
		} else {
			return null;
		}
	}

	public TodoHardCoded saveNewTodo(TodoHardCoded todo) {
//		if (todo.getId() == -1 || todo.getId() == 0) {

		if (todo != null) {
			todo.setId(++idCounter);
			todos.add(todo);
			return todo;
		} else {
			return null;
		}
	}
}
